#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ncurses.h>
#include <string.h>
#include "campo_minado.h"


void adiciona_bombas(struct dif *dif, struct cel jogo[dif->lin][dif->col]) {
    srand(time(NULL));
    int lin_aux, col_aux, i;
    i = 0;

    /*seta os valores*/
    for(int i = 0; i < dif->lin; i++) {
        for(int j = 0; j < dif->col; j++) {
            jogo[i][j].id = 0;
            jogo[i][j].reveal = FALSE; 
            jogo[i][j].flag = FALSE; 

        }
    }
    /*preenche com bombas*/
    while( i < dif->qtd_bombas) {
        lin_aux = rand() % dif->lin;
        col_aux = rand() % dif->col;
        if(jogo[lin_aux][col_aux].id != BOMBA) {
            jogo[lin_aux][col_aux].id = BOMBA;
            i++;
        }
    }

}
void conta_vizinhos(struct dif *dif, struct cel jogo[dif->lin][dif->col]) {
	int a, b, c, d;
    /*contabiliza*/
	for(a = 0; a <= dif->lin; a++) {
		for(b = 0; b <= dif->col; b++) {
			if((jogo[a][b].id != BOMBA) && (a < dif->lin) && (a > -1) && (b < dif->col) && (b > -1)) {
                for(c = a - 1; c <= a + 1; c++) {
                    for(d = b - 1; d <= b + 1; d++) {
                        if(jogo[c][d].id == BOMBA && c > -1 && c < dif->lin && d > -1 && d < dif->col) {
                            jogo[a][b].id++;
                        }
                    }
                }
            }
        }
    }
}

void gera_matriz(struct dif *dif, struct cel jogo[dif->lin][dif->col]) {
    adiciona_bombas(dif,jogo);
    conta_vizinhos(dif, jogo);    
}

int abre_celula(struct dif *dif, int lin, int col, struct cel jogo[dif->lin][dif->col]){ // lin e col serão selecionadas por setas
    if (jogo[lin][col].reveal == FALSE) {
        jogo[lin][col].reveal = TRUE; // Revela a celula
        if (jogo[lin][col].id == 0 && jogo[lin][col].id != BOMBA) {
        abre_celulas_adjacentes(dif, lin, col, jogo);
        return 1;
      }
      if(jogo[lin][col].id == BOMBA)
        return 0;     
    }
    return 1;
}

void abre_celulas_adjacentes(struct dif *dif, int lin, int col, struct cel jogo[dif->lin][dif->col]){
  if ((lin-1 >= 0) && (col-1 >= 0)){
    abre_celula(dif, lin-1, col-1, jogo);
  }
  if ((lin-1 >= 0)){
    abre_celula(dif, lin-1, col  , jogo);
  }
  if ((lin-1 >= 0) && (col+1 < dif->col)){
    abre_celula(dif, lin-1, col+1, jogo);
  }
  if (col-1 >= 0){
    abre_celula(dif, lin  , col-1, jogo);
  }
  if (col+1 < dif->col){
    abre_celula(dif, lin  , col+1, jogo);
  }
  if ((lin+1 < dif->lin) && (col-1 >= 0)){
    abre_celula(dif, lin+1, col-1, jogo);
  }
  if (lin+1 < dif->lin){
    abre_celula(dif, lin+1, col  , jogo);
  }
  if ((lin+1 < dif->lin) && (col+1 < dif->col)){
    abre_celula(dif, lin+1, col+1, jogo);
  }
}

int checa_derrota(struct dif *dif, struct cel jogo[dif->lin][dif->col]){
    for(int i = 0; i < dif->lin; i++) {
        for(int j = 0; j < dif->col; j++) {
            if ( (jogo[i][j].id == -1) && (jogo[i][j].reveal == 1) ){
            return 1;
          }
        }
    }
    return 0;
}

int checa_vitoria(struct dif *dif, struct cel jogo[dif->lin][dif->col]){
    for(int i = 0; i < dif->lin; i++) {
        for(int j = 0; j < dif->col; j++) {
            if ((jogo[i][j].reveal == FALSE) && (jogo[i][j].id != BOMBA)) {
            return 0;
            }
        }
    }
  return 1;
}

void imprime_jogo(struct dif *dif, struct cel jogo[dif->lin][dif->col], WINDOW *win, int continua, int *cel_reveladas){ 
    int i,j;

    if(!continua) {
        /* pra calcular o score */
        for(i = 0; i < dif->lin; i++) {
            for(j = 0; j < dif->col; j++) {
                if(jogo[i][j].reveal == TRUE && jogo[i][j].id != BOMBA)
                  (*cel_reveladas)++;
            }
        }
        /* revela tudo se revelou uma bomba */ 
        for(i = 0; i < dif->lin; i++) {
            for(j = 0; j < dif->col; j++){
                jogo[i][j].reveal = TRUE;                       
            }
        }
    }

    for(i = 0; i < dif->lin; i++) {
        for(j = 0; j < dif->col; j++){
            wmove(win, i+1 , j*3+1);                         
            imprime_char(dif, jogo[i][j], win);                        
        }
    }
  wrefresh(win);  
}

void imprime_char(struct dif *dif, struct cel celula, WINDOW *win) {

    start_color();
    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_RED, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN, COLOR_BLACK);

    if (celula.reveal == FALSE){
        if(celula.flag == TRUE) {
            wattron(win, COLOR_PAIR(6)); // Ciano
            waddch(win, '!');
            wattroff(win, COLOR_PAIR(6));
        }
        else
            waddch(win, '.');
    }
    else
    {

        switch (celula.id)
        {

        case (-1):
            wattron(win, COLOR_PAIR(5)); // Magenta
            waddch(win, '*');
            wattroff(win, COLOR_PAIR(5));
            break;

        case (0):
            waddch(win, ' ');
            break;

        case(1):
            wattron(win, COLOR_PAIR(1)); // AZUL
            waddch(win, '1');
            wattroff(win, COLOR_PAIR(1));
            break;

        case(2):
            wattron(win, COLOR_PAIR(2)); // MAGENTA
            waddch(win, '2');
            wattroff(win, COLOR_PAIR(2));
            break;


        case(3):
            wattron(win, COLOR_PAIR(3)); // Amarelo
            waddch(win, '3');
            wattroff(win, COLOR_PAIR(3));
            break;

        case(4):
            wattron(win, COLOR_PAIR(4)); // Vermelho
            waddch(win, '4');
            wattroff(win, COLOR_PAIR(4));
            break;

        case(5):
            wattron(win, COLOR_PAIR(4)); // Vermelho
            waddch(win, '5');
            wattroff(win, COLOR_PAIR(4));
            break;

        case(6):
            wattron(win, COLOR_PAIR(4)); // Vermelho
            waddch(win, '6');
            wattroff(win, COLOR_PAIR(4));
            break;

        case(7):
            wattron(win, COLOR_PAIR(4)); // Vermelho
            waddch(win, '7');
            wattroff(win, COLOR_PAIR(4));
            break;

        case(8):
            wattron(win, COLOR_PAIR(4));  // Vermelho
            waddch(win, '8');
            wattroff(win, COLOR_PAIR(4));
            break;
        }

    }
}
void escolha_usuario(struct dif *dif, struct cel jogo[dif->lin][dif->col]) {
    /* Inicialização */
    initscr();
    cbreak();
    curs_set(1);
    noecho();
    start_color();
    /*define valores*/
    int choice, xMax, yMax, x, y, base_max_X, base_max_Y,lin, col, continua, ganhou,score, tentativas,cel_reveladas;
    xMax = dif->col;
    yMax = dif->lin;
    x = y = 1; 
    continua = 1;
    ganhou = 0;
    score = tentativas = cel_reveladas = 0;    
    getmaxyx(stdscr, base_max_Y, base_max_X);
    WINDOW *base = newwin(base_max_Y, base_max_X, 0, 0);     
    WINDOW *win = newwin(yMax+2, xMax + 2*(xMax), 1,1);
    box(win, 0, 0);
    wmove(win,1,1);
    refresh();
    keypad(win,true);
    wrefresh(win);

    imprime_jogo(dif, jogo, win, continua, &cel_reveladas); 
    wmove(win, 1 ,1 );
    /*movimentação*/
    do
    {   
        choice = wgetch(win); 
        /* pega o movimento do jogador */
        switch(choice)
        {
            case KEY_UP:            
                y--;
                if(y < 1)
                    y = 1;
                wmove(win,y,x);
                break;
            case KEY_DOWN:
                y++;
                if(y > yMax)
                    y = yMax;
                wmove(win,y,x);
                break;
            case KEY_LEFT:                
                x= x-3;
                if(x < 1)
                    x = 1;
                wmove(win,y,x);
                break;
            case KEY_RIGHT:                
                x=x+3;
                if(x > (xMax + 2*(xMax))-2)
                    x = (xMax + 2*(xMax))-2;
                wmove(win,y,x);
                break;
            case 10: //ascii para enter                
                lin = y -1; // -1 pra matriz
                col = x-((x/3)*2) -1;// -1 pra matriz 

                continua = abre_celula(dif, lin, col, jogo);
                ganhou = checa_vitoria(dif, jogo);
                if(continua) {
                    tentativas++;
                }            
                imprime_jogo(dif, jogo, win, continua, &cel_reveladas);
                wrefresh(win);                  
                wmove(win,y,x);
                break;
             case 104: //ascii para h
                 /* limpa a tela */               
                 wclear(win);
                 wrefresh(win);
                 /* escreve a ajuda */
                 mvwprintw(base,0,0 ,"BEM-VINDO A AJUDA DO CAMPO-MINADO:\n -PRESSINE 'ENTER' PARA ESCOLHER A CELULA\n -PRESSIONE 'f' PARA POSICIONAR OU RETIRAR UMA BANDEIRA\n -USE AS SETAS DO TECLADO PARA MOVIMENTAR-SE\n -OS NUMEROS INDICAM A QUANTIDADE DE BOMBAS AO REDOR DA CELULA\n -SEU OBJETIVO EH REVELAR TODAS AS CELULAS SEM BOMBA, CASO REVELE UMA BOMBA, PERDERA\n\n --PRESSIONE QUALQUER TECLA PARA SAIR--");
                 wrefresh(base);
                 wgetch(base); 
                 imprime_jogo(dif, jogo, win, continua, &cel_reveladas);                
                 wclear(base);
                 wrefresh(base);                 
                 break; 
             case 102: //ascii para f
                lin = y -1; // -1 pra matriz
                col = x-((x/3)*2) -1; // -1 pra matriz
                if(jogo[lin][col].flag == FALSE)
                    jogo[lin][col].flag = TRUE;
                else
                    jogo[lin][col].flag = FALSE;
                imprime_jogo(dif, jogo, win, continua, &cel_reveladas);
                wrefresh(win);                  
                wmove(win,y,x);              
                 break; 
            default:
                break;
        }
        /* pra quando chamar ajudar n ficar bugado */         
        box(win, 0, 0);
        wmove(win,y,x);
        wrefresh(win);
    }
        while(continua && !ganhou); 
    wgetch(win);   
    /* tratamento do score */ 
    if(tentativas == 0) {
        score = 0;
    }
    else {
        score = (cel_reveladas/tentativas)*10;
    }      
    FILE *arq ;  
    arq = fopen ("scores.txt", "a") ;
    fprintf(arq, "\nscore: %d",score);
    fclose(arq);

    if(ganhou){
        wclear(base);        
        wprintw(base,"VOCE GANHOU, PARABENS! SCORE: %d\n PRESSIONE QUALQUER TECLA PARA SAIR", score); // função do score
        wrefresh(base);
        curs_set(0);
        
    }
    else {
        wclear(base);
        wprintw(base,"VOCE PERDEU! SCORE: %d\n PRESSIONE QUALQUER TECLA PARA SAIR", score); //função do score
        wrefresh(base);
        curs_set(0);        
    }

    wrefresh(win);
    wgetch(win);
    endwin();
 }

